## Image Upload and Download microservice

Using a handler to deal with the upload files and save them asynchronously with the help of tokio crate.

![imgUpload](https://raw.githubusercontent.com/guisithos/microservice/main/base_files/uploads/Captura%20de%20tela%20de%202022-06-30%2014-25-15.png)

![imgDownload](https://raw.githubusercontent.com/guisithos/microservice/main/base_files/uploads/Captura%20de%20tela%20de%202022-06-30%2014-26-49.png)
